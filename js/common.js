
var modal = {
	container: null,
	open: function(selector, callback) {
		$(selector).modal({
			overlayId:    'modal_overlay',
			containerId:  'modal_container',
			closeHTML:    null,
			opacity:      75,
			// position:     [300,],
			overlayClose: true,
			onOpen:       function(d){
				modal.onOpen(d, callback);
			},
			onClose:      modal.onClose
		});

		return false;
	},
	onOpen: function (d, callback) {
		var self = this;
		self.container = d.container;
		d.overlay.fadeIn(300, function () {
			var $title = $("div.modal-title", self.container);
			var $data  = $("div.modal-data", self.container);
			var $close = $("div.modal-close", self.container);

			$close.click(function(){
				$.modal.close();
			});
			$('#modal_container').find('.simplemodal-wrap').css({padding:0});
			d.container.css({height:0}).show();
			$(".modal-content", self.container).show();
			$close.show();

			$title.show();
			$data.show();

			setTimeout(function () {
				self.resize();

				if (callback) {
					callback.call(self);
				}
			}, 100);
		});
	},
	resize: function(newHeight) {
		var self = this;
		if (!newHeight) {
			var $title = $("div.modal-title", self.container);
			var $data  = $("div.modal-data", self.container);
			newHeight = $data.outerHeight() + $title.outerHeight();
		}
		self.container.css({top: $(window).height()/2 - newHeight/2}).animate({height: newHeight}, 300);
	},
	onClose: function (d) {
		// var self = this; // this = SimpleModal object
		d.container.animate(
			{top:"-" + (d.container.outerHeight()), opacity:0},
			300,
			function () {
				$.modal.close();
			}
		);
	}
};