<?php

$thumb_size  = 300;
$thumb_space = 40;
$screen_size = 1200;

$scale = number_format($thumb_size/$screen_size, 2)

?><!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>Gordo.st</title>
<style>
	body {
		margin:     50px auto;
		background: #555;
		width: <?=floor(1000/$thumb_size) * ($thumb_size + $thumb_space)  ?>px;
	}
	body > div {
		width:           <?=$thumb_size ?>px;
		height:          <?=$thumb_size ?>px;
		overflow:        hidden;
		position:        relative;
		background:      #DDD;
		float:           left;
		margin:          <?=intval($thumb_space/2) ?>px;
		box-shadow:      0 1px 5px rgba(0,0,0,.5), 0 0 0 5px rgba(0,0,0,.1);
	}
	body > div > iframe {
		margin:                   0;
		width:                    <?=$screen_size ?>px;
		height:                   <?=$screen_size ?>px;
		overflow:                 hidden;
		-moz-transform:           scale(<?=$scale ?>);
		-moz-transform-origin:    0 0;
		-o-transform:             scale(<?=$scale ?>);
		-o-transform-origin:      0 0;
		-webkit-transform:        scale(<?=$scale ?>);
		-webkit-transform-origin: 0 0;
	}
	body > div > a {
		display: block;
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		box-shadow: inset 0 1px 50px rgba(0,0,0,.3), inset 0 1px 3px rgba(255,255,255,.3);
	}
</style>
</head>
<body>
<? $files = scandir(__DIR__) ?>
<? foreach ($files as $file): ?>
	<? if (preg_match('@\.html$@', $file)): ?>
		<div>
			<iframe src="<?=$file ?>"  frameborder="0"></iframe>
			<a href="<?=$file ?>"></a>
		</div>
	<? endif ?>
<? endforeach ?>
</body>
</html>